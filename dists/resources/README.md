# JOD PC Linux - 1.0

Documentation for JOD PC Linux. This JOD Distribution allow to startup and manage a JOD Agent that represent a Linux computer.


## JOD Distribution Specs

This JOD Distribution was created and maintained as part of the John O.S. Project.

|||
|---|---|
| **Current version**    | 1.0
| **References**        | [JOD PC Linux @ JOSP Docs](https://www.johnosproject.org/docs/references/jod_dists/jod_pc_linux/)
| **Repository**        | [com.robypomper.josp.jod.pc.linux @ Bitbucket](https://bitbucket.org/johnosproject_shared/com.robypomper.josp.jod.pc.linux/)
| **Downloads**            | [com.robypomper.josp.jod.pc.linux > Downloads @ Bitbucket](https://bitbucket.org/johnosproject_shared/com.robypomper.josp.jod.pc.linux/downloads/)

This distribution can be installed on a Linux computer, and then it starts sharing computer's features to the JOSP EcoSystem like te volume control, resources
(cpu, memory, disks...) states, etc...


## JOD Distribution Usage

### Locally JOD Instance

Each JOD Distribution comes with a set of script for local JOD Instance management.

| Command | Description |
|---------|-------------|
| Start    <br/>```$ bash start.sh```     | Start local JOD instance in background mode, logs can be retrieved via ```tail -f logs/console.log``` command |
| Stop     <br/>```$ bash stop.sh```      | Stop local JOD instance, if it's running |
| State    <br/>```$ bash state.sh```     | Print the local JOD instance state (obj's id and name, isRunning, PID...) |
| Install  <br/>```$ bash install.sh```   | Install local JOD instance as system daemon/service |
| Uninstall<br/>```$ bash uninstall.sh``` | Uninstall local JOD instance as system daemon/service |

### Remote JOD Instance

To deploy and manage a JOD instance on remote device (local computer, cloud server,
object device...) please use the [John Object Remote](https://www.johnosproject.org/docs/references/tools/john_object_remote/)
tools.