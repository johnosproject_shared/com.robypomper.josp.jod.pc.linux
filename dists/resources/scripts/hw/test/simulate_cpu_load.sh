#!/bin/bash

################################################################################
# The John Operating System Project is the collection of software and configurations
# to generate IoT EcoSystem, like the John Operating System Platform one.
# Copyright (C) 2021 Roberto Pompermaier
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
################################################################################

fullload1() {
  echo "Fill 1 CPU(s)"
  dd if=/dev/zero of=/dev/null &
}

fullload2() {
  echo "Fill 2 CPU(s)"
  dd if=/dev/zero of=/dev/null | dd if=/dev/zero of=/dev/null &
}

fullload4() {
  echo "Fill 4 CPU(s)"
  dd if=/dev/zero of=/dev/null | dd if=/dev/zero of=/dev/null | dd if=/dev/zero of=/dev/null | dd if=/dev/zero of=/dev/null &
}

fullload8() {
  echo "Fill 8 CPU(s)"
  dd if=/dev/zero of=/dev/null | dd if=/dev/zero of=/dev/null | dd if=/dev/zero of=/dev/null | dd if=/dev/zero of=/dev/null |
    dd if=/dev/zero of=/dev/null | dd if=/dev/zero of=/dev/null | dd if=/dev/zero of=/dev/null | dd if=/dev/zero of=/dev/null &
}

[ -n "$1" ] && C=$1 || C=1

[ "$C" -ne 1 ] && [ "$C" -ne 2 ] && [ "$C" -ne 4 ] && [ "$C" -ne 8 ] && echo "Invalid param, valid values are 1, 2, 4 or 8, exit" && exit

[ "$C" -eq 1 ] && fullload1
[ "$C" -eq 2 ] && fullload2
[ "$C" -eq 4 ] && fullload4
[ "$C" -eq 8 ] && fullload8

echo "Enter to kill all heavy process"
read

killall dd
