#!/bin/bash

################################################################################
# The John Operating System Project is the collection of software and configurations
# to generate IoT EcoSystem, like the John Operating System Platform one.
# Copyright (C) 2021 Roberto Pompermaier
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
################################################################################

# Detect main control
[ -n "$(amixer scontrols | grep Master)" ] &&
  CONTROL=Master ||
  CONTROL=$(amixer scontrols | head -1 | awk -F"'" '{print $2}')

# If give a param, then set mute
if [ -n "$1" ]; then
  [[ "$1" == "false" || "$1" == "0" ]] && VAL=on || VAL=off
  amixer -qD pulse sset $CONTROL $VAL 2>/dev/null
  [ $? -gt 0 ] && amixer -q sset $CONTROL $VAL 2>/dev/null
  exit
fi

# Get $CONTROL status
AMIXER=$(amixer -D pulse get $CONTROL 2>/dev/null)
[ -z "$AMIXER" ] && AMIXER=$(amixer get $CONTROL 2>/dev/null)

# Extract $CONTROL volume
VOL=$(echo "$AMIXER" | grep 'Front Left: Playback' | awk -F'[][]' '{ print $4 }')
[ -z "$VOL" ] && VOL=$(echo $AMIXER | grep 'Mono: Playback' | awk -F'[][]' '{ print $4 }')

[ "$VOL" = "off" ] && echo true || echo false
